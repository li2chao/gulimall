package com.atguigu.gulimall.member;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.atguigu.gulimall.member.service.MemberService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class GulimallMemberApplicationTests {

    @Autowired
    MemberService memberService;

    @Test
    void contextLoads() {
        //MemberEntity memberEntity = new MemberEntity();
        //memberEntity.setUsername("张三");
        //memberService.save(memberEntity);
        //System.out.println("保存成功");
        MemberEntity memberEntity = memberService.getById(1L);
        System.out.println(memberEntity);
    }

}
