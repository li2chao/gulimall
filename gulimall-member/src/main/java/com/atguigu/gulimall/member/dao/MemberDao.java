package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 15:39:16
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
