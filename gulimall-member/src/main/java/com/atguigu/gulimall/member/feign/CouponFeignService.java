package com.atguigu.gulimall.member.feign;

import com.atguigu.gulimall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Name CouponFeignService
 * @Description: 声明式服务远程调用接口 - 优惠券服务
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2023-12-23 20:27:16
 **/
@FeignClient("gulimall-coupon")
public interface CouponFeignService {

    @RequestMapping("/coupon/coupon/list")
    R getMemberCoupons();

}
