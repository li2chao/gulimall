package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品三级分类
 * 
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 08:51:28
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {

    /**
     * 更新分类信息
     * @param catId 分类 ID
     * @param catName 分类名称
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/02 19:18:00
     */
    void updateCategory(@Param("catId") Long catId, @Param("catName") String catName);

}
