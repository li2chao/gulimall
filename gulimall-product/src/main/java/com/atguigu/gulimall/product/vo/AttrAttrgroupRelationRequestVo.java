package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @Name AttrAttrgroupRelationRequestVo
 * @Description: [属性-属性分组-关联表]请求值对象
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-04 15:46:18
 **/
@Data
public class AttrAttrgroupRelationRequestVo {
    /**
     * 属性 ID
     */
    private Long attrId;
    /**
     * 属性分组 ID
     */
    private Long attrGroupId;

}

