package com.atguigu.gulimall.product.service.impl;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.Query;
import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.CategoryBrandRelationEntity;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.IntFunction;
import java.util.stream.Collectors;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(new Query<CategoryEntity>().getPage(params), new QueryWrapper<CategoryEntity>());

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        // 1. 获取所有的分类条目
        List<CategoryEntity> all = baseMapper.selectList(null);

        // 2. 查询出所有分类的子分类，并对其进行排序，最终得到三级有序分类
        List<CategoryEntity> result = all.stream()
                // 2.1 查询出所有的一级分类
                .filter((categoryEntity) -> {
                    return categoryEntity.getParentCid() == 0;
                })
                // 2.2 对一级分类进行排序
                .sorted((c1, c2) -> {
                    return (c1.getSort() == null ? 0 : c1.getSort()) - (c2.getSort() == null ? 0 : c2.getSort());
                })
                // 2.3 设置一级分类的子分类
                .map((categoryEntity) -> {
                    categoryEntity.setChildren(getChildren(categoryEntity, all));
                    return categoryEntity;
                })
                // 2.4 收集结果
                .collect(Collectors.toList());

        return result;
    }

    /**
     * 递归查询所有的子分类
     * @param root 根分类
     * @param all 所有分类
     * @return java.util.List<com.atguigu.gulimall.product.entity.CategoryEntity>
     * @author lichao
     * @email li2chao@126.com
     * @date 2023/12/24 20:39:57
     */
    private List<CategoryEntity> getChildren(CategoryEntity root, List<CategoryEntity> all) {
        List<CategoryEntity> children = all.stream()
                // 1. 获取所有根分类的子分类
                .filter((categoryEntity) -> {
                    return categoryEntity.getParentCid() == root.getCatId();
                })
                // 2. 对所有子分类进行排序
                .sorted((s1, s2) -> {
                    return (s1.getSort() == null ? 0 : s1.getSort()) - (s2.getSort() == null ? 0 : s2.getSort());
                })
                // 3. 递归设置所有子分类的子分类
                .map((categoryEntity) -> {
                    categoryEntity.setChildren(getChildren(categoryEntity, all));
                    return categoryEntity;
                })
                // 4. 收集结果
                .collect(Collectors.toList());

        return children;
    }

    @Override
    public void removeCategoryByIds(List<Long> list) {
        // TODO 检查当前删除的分类条目（菜单）是否被别的地方引用？

        baseMapper.deleteBatchIds(list);
    }

    @Override
    public Long[] findCategoryPath(Long categoryId) {
        List<Long> path = new ArrayList<>();

        // 1. 获取当前分类条目的所有父 ID 集合
        path = getParentCid(categoryId, path);
        // 2. 反转，得到完整的分类路径
        Collections.reverse(path);

        return (Long[]) path.toArray(new Long[path.size()]);
    }

    /**
     * 获取当前分类的所有父 ID 集合
     * @param categoryId 当前分类 ID
     * @param path 父 ID 集合
     * @return java.util.List<java.lang.Long> 所有父 ID 集合
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/02 16:27:06
     */
    private List<Long> getParentCid(Long categoryId, List<Long> path) {
        path.add(categoryId);
        Long parentCid = this.getById(categoryId).getParentCid();
        if(parentCid != 0) {
            getParentCid(parentCid, path);
        }
        return path;
    }

    // 事务处理
    @Transactional
    @Override
    public void updateCascade(CategoryEntity category) {
        // 1. 更新当前分类信息
        this.updateById(category);

        if(StringUtils.isNotBlank(category.getName())) {
            // 2. 更新 [分类-品牌-关联表] 中的冗余信息 [分类名称]
            long count = categoryBrandRelationService.count(
                    new QueryWrapper<CategoryBrandRelationEntity>().eq("catelog_id", category.getCatId()));
            if(count > 0) {
                categoryBrandRelationService.updateCategoryName(category.getCatId(), category.getName());
            }

            // TODO 更新当前分类的其他关联信息
            // ... ...
        }
    }


}