package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.CategoryBrandRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 品牌分类关联
 *
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 08:51:28
 */
@Mapper
public interface CategoryBrandRelationDao extends BaseMapper<CategoryBrandRelationEntity> {

    /**
     * 更新品牌信息
     * @param brandId 品牌 ID
     * @param brandName 品牌名称
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/02 19:26:04
     */
    void updateBrand(@Param("brandId") Long brandId, @Param("brandName") String brandName);

    /**
     * 更新分类信息
     * @param catelogId 分类 ID
     * @param catelogName 分类名称
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/02 19:26:24
     */
    void updateCategory(@Param("catelogId") Long catelogId, @Param("catelogName") String catelogName);
}
