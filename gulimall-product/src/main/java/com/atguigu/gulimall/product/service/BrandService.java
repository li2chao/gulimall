package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 品牌
 *
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 08:51:28
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 级联更新
     * 1. 更新自身信息
     * 2. 更新关联表中的冗余信息
     * @param brand 待更新信息
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/02 19:11:51
     */
    void updateCascade(BrandEntity brand);
}

