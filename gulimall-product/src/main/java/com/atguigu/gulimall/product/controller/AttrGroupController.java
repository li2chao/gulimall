package com.atguigu.gulimall.product.controller;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.R;
import com.atguigu.gulimall.common.validator.group.AddGroup;
import com.atguigu.gulimall.common.validator.group.UpdateGroup;
import com.atguigu.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.entity.AttrGroupEntity;
import com.atguigu.gulimall.product.service.AttrAttrgroupRelationService;
import com.atguigu.gulimall.product.service.AttrGroupService;
import com.atguigu.gulimall.product.service.AttrService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.AttrAttrgroupRelationRequestVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 属性分组
 *
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 08:51:28
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AttrService attrService;
    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;

    /**
     * 获取属性分组列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("generator:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = attrGroupService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 获取指定分类下的属性分组列表
     */
    @RequestMapping("/list/{catelogId}")
    public R listByCatelogId(
            @RequestParam Map<String, Object> params,
            @PathVariable("catelogId") Long catelogId) {
        PageUtils page = attrGroupService.listByCatelogId(params, catelogId);

        return R.ok().put("page", page);
    }

    /**
     * 获取指定分类下的属性分组以及关联属性列表
     */
    @RequestMapping("/{catelogId}/withattr")
    public R listWithAttrByCatelogId(@PathVariable("catelogId") Long catelogId) {
        List<AttrGroupEntity> data = attrGroupService.listWithAttrByCatelogId(catelogId);

        return R.ok().put("data", data);
    }

    /**
     * 获取指定分组下的属性列表
     */
    @GetMapping("/{attrgroupId}/attr/relation")
    public R listAttrByAttrGroupId(@PathVariable("attrgroupId") Long attrgroupId) {
        List<AttrEntity> data = attrService.listByAttrGroupId(attrgroupId);

        return R.ok().put("data", data);
    }

    /**
     * 获取属性分组里面还没有关联的其他基本属性
     */
    @GetMapping("/{attrgroupId}/noattr/relation")
    public R listBaseAttrByNoAttrGroupId(
            @RequestParam Map<String, Object> params,
            @PathVariable("attrgroupId") Long attrgroupId) {
        PageUtils page = attrService.listBaseAttrByNoAttrGroupId(params, attrgroupId);

        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    // @RequiresPermissions("generator:attrgroup:info")
    public R info(@PathVariable("attrGroupId") Long attrGroupId) {
        AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);

        // 设置属性分组所属的分类条目的完整 ID 路径
        // 例如: [1, 25, 235]
        Long catelogId = attrGroup.getCatelogId();
        Long[] catelogPath = categoryService.findCategoryPath(catelogId);
        attrGroup.setCatelogPath(catelogPath);

        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("generator:attrgroup:save")
    public R save(@Validated(AddGroup.class) @RequestBody AttrGroupEntity attrGroup) {
        attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 保存 [属性-属性分组-关联表] 信息
     */
    @RequestMapping("/attr/relation")
    public R saveAttrRelation(@RequestBody AttrAttrgroupRelationRequestVo[] relations) {
        attrAttrgroupRelationService.saveBatchRelations(relations);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("generator:attrgroup:update")
    public R update(@Validated(UpdateGroup.class) @RequestBody AttrGroupEntity attrGroup) {
        attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("generator:attrgroup:delete")
    public R delete(@RequestBody Long[] attrGroupIds) {
        attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

    /**
     * 删除 [属性-属性分组-关联表] 信息
     */
    @RequestMapping("/attr/relation/delete")
    public R deleteRelationByAttrId(@RequestBody AttrAttrgroupRelationRequestVo[] relations) {
        attrAttrgroupRelationService.removeBatchRelations(relations);

        return R.ok();
    }

}
