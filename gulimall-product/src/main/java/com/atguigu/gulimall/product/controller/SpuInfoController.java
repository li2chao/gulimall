package com.atguigu.gulimall.product.controller;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.R;
import com.atguigu.gulimall.product.entity.SpuInfoEntity;
import com.atguigu.gulimall.product.service.SpuInfoService;
import com.atguigu.gulimall.product.vo.save.SpuSaveVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * spu信息
 *
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 08:51:28
 */
@RestController
@RequestMapping("product/spuinfo")
public class SpuInfoController {
    @Autowired
    private SpuInfoService spuInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("generator:spuinfo:list")
    public R list(@RequestParam Map<String, Object> params) {
        // PageUtils page = spuInfoService.queryPage(params);
        PageUtils page = spuInfoService.queryPageByCondition(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("generator:spuinfo:info")
    public R info(@PathVariable("id") Long id) {
        SpuInfoEntity spuInfo = spuInfoService.getById(id);

        return R.ok().put("spuInfo", spuInfo);
    }

    /**
     * 保存商品的所有信息（核心）
     * 1. 保存 spu 的基本信息 pms_spu_info
     * 2. 保存 spu 的描述图片 pms_spu_info_desc
     * 3. 保存 spu 的图片集 pms_spu_images
     * 4. 保存 spu 的规格参数 pms_product_attr_valye
     * 5. 保存 spu 的会员（成长、购物等）积分信息
     *    gulimall-sms -> sms_spu_bounds
     * 6. 保存当前 spu 对用的所有 sku 信息:
     *     6.1 保存 sku 的基本信息 pms_sku_info
     *     6.2 保存 sku 的图片信息 pms_sku_images
     *     6.3 保存 sku 的销售属性信息 pms_sku_sale_attr_value
     *     6.4 保存 sku 的优惠、满减、价格等信息
     *         gulimall_sms -> 会员优惠信息 sms_sku_ladder
     *                      -> 会员满减信息 sms_sku_full_reduction
     *                      -> 会员价格信息 sms_member_price
     * 注意事项：
     * 1. Controller 层将数据提交到 Service 层进行业务处理前，需要进行参数校验；
     * 2. Service 业务层进行数据处理时需要开启事务，保证操作的原子性。
     * [遗留知识点] 处理保存成功容易，处理保存失败难。
     * 1. 处理保存过程中，如果遇到异常，怎么处理？？？
     * 2. 如果保存失败，如何将数据（状态）回滚至保存前的状态？？？
     */
    @RequestMapping("/save")
    // @RequiresPermissions("generator:spuinfo:save")
    public R save(@RequestBody SpuSaveVo spuInfo) {
        // spuInfoService.save(spuInfo);

        spuInfoService.saveSpuInfo(spuInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("generator:spuinfo:update")
    public R update(@RequestBody SpuInfoEntity spuInfo) {
        spuInfoService.updateById(spuInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("generator:spuinfo:delete")
    public R delete(@RequestBody Long[] ids) {
        spuInfoService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
