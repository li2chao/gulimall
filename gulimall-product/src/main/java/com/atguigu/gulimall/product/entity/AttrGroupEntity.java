package com.atguigu.gulimall.product.entity;

import com.atguigu.gulimall.common.validator.group.AddGroup;
import com.atguigu.gulimall.common.validator.group.UpdateGroup;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 属性分组
 *
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 08:51:28
 */
@Data
@TableName("pms_attr_group")
public class AttrGroupEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 分组id
     */
    @Null(groups = UpdateGroup.class)
    @NotNull(groups = AddGroup.class)
    @TableId
    private Long attrGroupId;
    /**
     * 组名
     */
    @NotNull(groups = AddGroup.class)
    private String attrGroupName;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 描述
     */
    private String descript;
    /**
     * 组图标
     */
    @NotNull(groups = AddGroup.class)
    private String icon;
    /**
     * 所属分类id
     */
    @NotNull(groups = AddGroup.class)
    private Long catelogId;
    /**
     * 所属分类的（三级）完整路径
     * 例如: [2, 25, 255]
     */
    // 当该字段内容不为空时，才显式返回该字段及其内容
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @TableField(exist = false)
    private Long[] catelogPath;
    /**
     * 当前分组下的所有属性
     */
    @TableField(exist = false)
    private List<AttrEntity> attrs;

}
