package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 08:51:28
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

    void removeCategoryByIds(List<Long> list);

    /**
     * 获取当前分类条目的完整 ID 路径
     * 例如: [1, 25, 235]
     * @param categoryId 当前分类条目 ID
     * @return java.lang.Long[]
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/02 16:23:14
     */
    Long[] findCategoryPath(Long categoryId);

    /**
     * 级联更新
     * 1. 更新自身信息
     * 2. 更新关联表中的冗余信息
     * @param category 待更新信息
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/02 19:11:10
     */
    void updateCascade(CategoryEntity category);
}

