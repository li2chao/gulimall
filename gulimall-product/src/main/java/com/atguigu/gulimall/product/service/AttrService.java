package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.vo.AttrRequestVo;
import com.atguigu.gulimall.product.vo.AttrResponseVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 08:51:28
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 获取商品属性列表
     * @param attrType 属性类型
     *        sale = 0 = 销售属性, 且销售属性不存在所属分组信息
     *        base = 1 = 基本属性（规格参数）
     *        both = 2 = 既是销售属性又是基本属性（规格参数）
     * @param params 分页参数
     * @param catelogId 所属分类
     * @return com.atguigu.gulimall.common.utils.PageUtils 结果
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/03 17:37:37
     */
    PageUtils listByCatelogId(String attrType, Map<String, Object> params, Long catelogId);

    /**
     * 获取属性的详细信息（包含属性分组ID、非销售属性所属分类名称等）
     * @param attrId 属性 ID
     * @return com.atguigu.gulimall.product.vo.AttrResponseVo 结果
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/03 19:13:43
     */
    AttrResponseVo getDetailById(Long attrId);

    /**
     * 级联保存
     * 1. 保存属性信息
     * 2. 如果不是销售属性，保存 [属性-属性分组-关联表] 信息
     * @param attr 基本属性的请求值对象信息
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/03 19:31:05
     */
    void saveCascade(AttrRequestVo attr);

    /**
     * 级联更新
     * 1. 更新属性信息
     * 2. 如果不是销售属性，更新 [属性-属性分组-关联表] 信息
     * @param attr 基本属性的请求值对象信息
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/03 19:31:05
     */
    void updateCascade(AttrRequestVo attr);

    /**
     * 级联删除
     * 1. 删除属性信息
     * 2. 如果不是销售属性，删除 [属性-属性分组-关联表] 信息
     * @param attrIds 属性 ID 集合
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/03 21:19:57
     */
    void removeCascadeByIds(List<Long> attrIds);

    /**
     * 获取指定分组关联的所有属性
     * @param attrgroupId 分组 ID
     * @return java.util.List<com.atguigu.gulimall.product.entity.AttrEntity>
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/04 11:07:29
     */
    List<AttrEntity> listByAttrGroupId(Long attrgroupId);

    /**
     * 获取属性分组里面还没有关联的其他基本属性，方便添加新的关联
     * @param params 分页参数
     * @param attrgroupId 分组 ID
     * @return com.atguigu.gulimall.common.utils.PageUtils
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/04 13:46:54
     */
    PageUtils listBaseAttrByNoAttrGroupId(Map<String, Object> params, Long attrgroupId);
}

