package com.atguigu.gulimall.product.service.impl;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.Query;
import com.atguigu.gulimall.product.dao.ProductAttrValueDao;
import com.atguigu.gulimall.product.entity.ProductAttrValueEntity;
import com.atguigu.gulimall.product.service.ProductAttrValueService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


@Service("productAttrValueService")
public class ProductAttrValueServiceImpl extends ServiceImpl<ProductAttrValueDao, ProductAttrValueEntity> implements ProductAttrValueService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ProductAttrValueEntity> page = this.page(new Query<ProductAttrValueEntity>().getPage(params), new QueryWrapper<ProductAttrValueEntity>());

        return new PageUtils(page);
    }

    @Override
    public List<ProductAttrValueEntity> listBySpuId(Long spuId) {
        List<ProductAttrValueEntity> entities = baseMapper.selectList(new QueryWrapper<ProductAttrValueEntity>().eq("spu_id", spuId));

        return entities;
    }

    @Transactional
    @Override
    public void updateBatchBySpuId(Long spuId, List<ProductAttrValueEntity> productAttrValues) {
        // 1. 删除所有旧的商品基本属性值
        this.remove(new QueryWrapper<ProductAttrValueEntity>().eq("spu_id", spuId));

        // 2. 插入待更新的商品基本属性值
        List<ProductAttrValueEntity> productAttrValueEntities = productAttrValues.stream().map(value -> {
            value.setSpuId(spuId);
            return value;
        }).collect(Collectors.toList());
        this.saveBatch(productAttrValueEntities);
    }

}