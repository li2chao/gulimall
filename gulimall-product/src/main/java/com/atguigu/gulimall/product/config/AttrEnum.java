package com.atguigu.gulimall.product.config;

/**
 * @Name AttrEnum
 * @Description: 商品属性枚举
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-03 21:37:35
 **/
public enum AttrEnum {

    ATTR_TYPE_SALE(0, "sale"), ATTR_TYPE_BASE(1, "base"), ATTR_TYPE_BOTH(2, "both");

    private Integer code;
    private String name;

    AttrEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
