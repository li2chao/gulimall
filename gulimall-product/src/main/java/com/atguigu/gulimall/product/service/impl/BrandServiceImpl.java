package com.atguigu.gulimall.product.service.impl;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.Query;
import com.atguigu.gulimall.product.dao.BrandDao;
import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.entity.CategoryBrandRelationEntity;
import com.atguigu.gulimall.product.service.BrandService;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<BrandEntity> wrapper = new QueryWrapper<>();
        // 支持多字段模糊查询功能
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            wrapper.eq("brand_id", key).or().like("name", key);
        }
        IPage<BrandEntity> page = this.page(new Query<BrandEntity>().getPage(params), wrapper);
        return new PageUtils(page);
    }

    // 开启事务处理
    @Transactional
    @Override
    public void updateCascade(BrandEntity brand) {
        // 1. 更新品牌自己的信息
        this.updateById(brand);

        if(StringUtils.isNotBlank(brand.getName())) {
            // 2. 更新 [分类-品牌-关联表] 中的冗余信息 [品牌名称]
            long count = categoryBrandRelationService.count(
                    new QueryWrapper<CategoryBrandRelationEntity>().eq("brand_id", brand.getBrandId()));
            if(count > 0) {
                categoryBrandRelationService.updateBrandName(brand.getBrandId(), brand.getName());
            }

            // TODO 更新品牌关联的其他冗余信息
            // ... ...
        }

    }

}