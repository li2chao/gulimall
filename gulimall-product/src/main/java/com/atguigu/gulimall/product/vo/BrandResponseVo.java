package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @Name BrandResponseVo
 * @Description: [分类-品牌-关联表] 响应值对象
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-05 14:25:06
 **/
@Data
public class BrandResponseVo {
    /**
     * 品牌 ID
     */
    private Long brandId;
    /**
     * 品牌名称
     */
    private String brandName;
}

