package com.atguigu.gulimall.product.service.impl;

import com.atguigu.gulimall.common.dto.SkuFullReductionDto;
import com.atguigu.gulimall.common.dto.SkuLadderDto;
import com.atguigu.gulimall.common.dto.SkuMemberPriceDto;
import com.atguigu.gulimall.common.dto.SpuBoundsDto;
import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.Query;
import com.atguigu.gulimall.product.dao.SpuInfoDao;
import com.atguigu.gulimall.product.entity.*;
import com.atguigu.gulimall.product.feign.CouponFeignService;
import com.atguigu.gulimall.product.service.*;
import com.atguigu.gulimall.product.vo.SpuInfoResponseVo;
import com.atguigu.gulimall.product.vo.save.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


@Service("spuInfoService")
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    @Autowired
    private SpuInfoDescService spuInfoDescService;
    @Autowired
    private SpuImagesService spuImagesService;
    @Autowired
    private ProductAttrValueService productAttrValueService;
    @Autowired
    private AttrService attrService;
    @Autowired
    private SkuInfoService skuInfoService;
    @Autowired
    private SkuImagesService skuImagesService;
    @Autowired
    private SkuSaleAttrValueService skuSaleAttrValueService;
    @Autowired
    private CouponFeignService couponFeignService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private BrandService brandService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuInfoEntity> page = this.page(new Query<SpuInfoEntity>().getPage(params), new QueryWrapper<SpuInfoEntity>());

        return new PageUtils(page);
    }

    @Transactional
    @Override
    public void saveSpuInfo(SpuSaveVo spuSaveInfo) {
        // 1. 保存 spu 的基本信息 pms_spu_info
        SpuInfoEntity spuInfo = new SpuInfoEntity();
        BeanUtils.copyProperties(spuInfo, spuSaveInfo);
        spuInfo.setCreateTime(new Date());
        spuInfo.setUpdateTime(new Date());
        baseMapper.insert(spuInfo);
        // 取出 spu 信息的 ID 主键
        Long spuInfoId = spuInfo.getId();
        // 2. 保存 spu 的描述图片 pms_spu_info_desc
        SpuInfoDescEntity spuInfoDesc = new SpuInfoDescEntity();
        spuInfoDesc.setSpuId(spuInfoId);
        spuInfoDesc.setDecript(String.join(",", spuSaveInfo.getDecript()));
        spuInfoDescService.save(spuInfoDesc);
        // 3. 保存 spu 的图片集 pms_spu_images
        Optional<List<String>> spuImagesOptional = Optional.ofNullable(spuSaveInfo.getImages());
        spuImagesOptional.ifPresent(images -> {
            List<SpuImagesEntity> spuImagesEntities = images.stream()
                    .filter(image -> StringUtils.isNotBlank(image))
                    .map(image -> {
                        SpuImagesEntity spuImages = new SpuImagesEntity();
                        spuImages.setSpuId(spuInfoId);
                        spuImages.setImgUrl(image);
                        return spuImages;
                    }).collect(Collectors.toList());
            spuImagesService.saveBatch(spuImagesEntities);
        });
        // 4. 保存 spu 的规格参数 pms_product_attr_valye
        Optional<List<BaseAttrs>> baseAttrsOptional = Optional.ofNullable(spuSaveInfo.getBaseAttrs());
        baseAttrsOptional.ifPresent(baseAttrs -> {
            List<ProductAttrValueEntity> attrValueEntities = baseAttrs.stream().map(attr -> {
                ProductAttrValueEntity attrValue = new ProductAttrValueEntity();
                attrValue.setSpuId(spuInfoId);
                BeanUtils.copyProperties(attr, attrValue);
                // 设置规格参数的名称
                AttrEntity attrEntity = attrService.getById(attrValue.getAttrId());
                attrValue.setAttrName(attrEntity.getAttrName());
                attrValue.setQuickShow(attr.getShowDesc());
                return attrValue;
            }).collect(Collectors.toList());
            productAttrValueService.saveBatch(attrValueEntities);
        });
        // 5. 保存 spu 的会员（成长、购物等）积分信息
        // gulimall-sms -> sms_spu_bounds
        Bounds bounds = spuSaveInfo.getBounds();
        if (bounds.getBuyBounds().compareTo(new BigDecimal("0")) == 1 ||
                bounds.getGrowBounds().compareTo(new BigDecimal("0")) == 1) {
            SpuBoundsDto boundsDto = new SpuBoundsDto();
            boundsDto.setSpuId(spuInfoId);
            BeanUtils.copyProperties(bounds, boundsDto);
            couponFeignService.saveSpuBounds(boundsDto);
        }
        // 6. 保存当前 spu 对用的所有 sku 信息
        Optional<List<Skus>> skusOptional = Optional.ofNullable(spuSaveInfo.getSkus());
        skusOptional.ifPresent(skus -> {
            // 6.1 保存 sku 的基本信息 pms_sku_info
            skus.stream().forEach(sku -> {
                SkuInfoEntity skuInfo = new SkuInfoEntity();
                skuInfo.setSpuId(spuInfoId);
                BeanUtils.copyProperties(sku, skuInfo);
                skuInfo.setCatalogId(spuSaveInfo.getCatalogId());
                skuInfo.setBrandId(spuSaveInfo.getBrandId());
                skuInfo.setSkuDesc(String.join(",", sku.getDescar()));
                // 设置默认图片信息
                Optional<List<Images>> skuImagesOptional = Optional.ofNullable(sku.getImages());
                skuImagesOptional.ifPresent(skuImages -> {
                    skuImages.forEach(image -> {
                        if (image.getDefaultImg() == 1) {
                            skuInfo.setSkuDefaultImg(image.getImgUrl());
                        }
                    });
                });
                skuInfoService.save(skuInfo);
                //  取出 sku 信息的 ID 主键
                Long skuId = skuInfo.getSkuId();
                // 6.2 保存 sku 的图片信息 pms_sku_images
                skuImagesOptional.ifPresent(skuImages -> {
                    List<SkuImagesEntity> skuImagesEntities = skuImages.stream()
                            .filter(images -> StringUtils.isNotBlank(images.getImgUrl()))
                            .map(images -> {
                                SkuImagesEntity entity = new SkuImagesEntity();
                                entity.setSkuId(skuId);
                                entity.setImgUrl(images.getImgUrl());
                                entity.setDefaultImg(images.getDefaultImg());
                                return entity;
                            }).collect(Collectors.toList());
                    skuImagesService.saveBatch(skuImagesEntities);
                });
                // 6.3 保存 sku 的销售属性信息 pms_sku_sale_attr_value
                Optional<List<Attr>> skuAttrsOptional = Optional.ofNullable(sku.getAttr());
                skuAttrsOptional.ifPresent(skuAttrs -> {
                    List<SkuSaleAttrValueEntity> skuSaleAttrValueEntities = skuAttrs.stream().map(attr -> {
                        SkuSaleAttrValueEntity attrValue = new SkuSaleAttrValueEntity();
                        attrValue.setSkuId(skuId);
                        BeanUtils.copyProperties(attr, attrValue);
                        return attrValue;
                    }).collect(Collectors.toList());
                    skuSaleAttrValueService.saveBatch(skuSaleAttrValueEntities);
                });
                // 6.4 保存 sku 的优惠、满减、价格等信息
                // 6.4.1 保存会员优惠信息
                // gulimall_sms -> sms_sku_ladder
                if (sku.getFullCount() > 0) {
                    SkuLadderDto ladderDto = new SkuLadderDto();
                    ladderDto.setSkuId(skuId);
                    BeanUtils.copyProperties(sku, ladderDto);
                    ladderDto.setAddOther(sku.getCountStatus());
                    couponFeignService.saveSkuLadder(ladderDto);
                }
                // 6.4.2 保存会员满减信息
                // gulimall_sms -> sms_sku_full_reduction
                if (sku.getFullPrice().compareTo(new BigDecimal("0")) == 1) {
                    SkuFullReductionDto fullReductionDto = new SkuFullReductionDto();
                    fullReductionDto.setSkuId(skuId);
                    BeanUtils.copyProperties(sku, fullReductionDto);
                    fullReductionDto.setAddOther(sku.getPriceStatus());
                    couponFeignService.saveSkuFullReduction(fullReductionDto);
                }
                // 6.4.3 保存会员价格信息
                // gulimall_sms -> sms_sku_ladder
                Optional<List<MemberPrice>> memberPricesOptional = Optional.ofNullable(sku.getMemberPrice());
                memberPricesOptional.ifPresent(memberPrices -> {
                    List<SkuMemberPriceDto> memberPriceDtos = memberPrices.stream()
                            .filter(price -> price.getPrice().compareTo(new BigDecimal("0")) == 1)
                            .map(price -> {
                                SkuMemberPriceDto memberPriceDto = new SkuMemberPriceDto();
                                memberPriceDto.setSkuId(skuId);
                                memberPriceDto.setMemberLevelId(price.getId());
                                memberPriceDto.setMemberLevelName(price.getName());
                                memberPriceDto.setMembePrice(price.getPrice());
                                return memberPriceDto;
                            }).collect(Collectors.toList());
                    couponFeignService.saveBatchSkuMemberPrice(memberPriceDtos);
                });

            });
        });
    }

    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {
        QueryWrapper<SpuInfoEntity> queryWrapper = new QueryWrapper<>();
        // 支持多字段（SPU 标识 + 名称）联合查询
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            // 注意: 此处一定要用 and() 表达式连接起来，代表是一个 (子语句) 整体。
            queryWrapper.and(wrapper->{
                wrapper.eq("id", key).or().like("spu_name", key);
            });
        }
        // 支持按所属分类 ID 进行查询
        String catelogId = (String) params.get("catelogId");
        if (StringUtils.isNotBlank(catelogId) && !StringUtils.equals(catelogId, "0")) {
            queryWrapper.eq("catalog_id", catelogId);
        }
        // 支持按所属品牌 ID 进行查询
        String brandId = (String) params.get("brandId");
        if (StringUtils.isNotBlank(brandId) && !StringUtils.equals(catelogId, "0")) {
            queryWrapper.eq("brand_id", brandId);
        }
        // 支持按 SPU 显示状态进行查询
        String status = (String) params.get("status");
        if (StringUtils.isNotBlank(status)) {
            queryWrapper.eq("publish_status", status);
        }
        IPage<SpuInfoEntity> page = this.page(new Query<SpuInfoEntity>().getPage(params), queryWrapper);
        return new PageUtils(page);
    }

}