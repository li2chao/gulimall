package com.atguigu.gulimall.product.config;

/**
 * @Name Constants
 * @Description: 商品系统 - 常量
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-03 21:06:51
 **/
public interface ProductConstants {

    /**************属性类型****************/
    // 销售属性
    Integer ATTR_TYPE_SALE_CODE = 0;
    String ATTR_TYPE_SALE_NAME = "sale";
    // 基本属性（规格参数）
    Integer ATTR_TYPE_BASE_CODE = 1;
    String ATTR_TYPE_BASE_NAME = "base";
    // 既是销售属性又是基本属性（规格参数）
    Integer ATTR_TYPE_BOTH_CODE = 2;
    String ATTR_TYPE_BOTH_NAME = "both";
    /*************************************/





}
