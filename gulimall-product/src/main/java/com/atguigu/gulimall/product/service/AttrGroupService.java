package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 08:51:28
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils listByCatelogId(Map<String, Object> params, Long catelogId);

    /**
     * 获取指定分类下的属性分组以及关联属性列表
     *
     * @param catelogId 分类 ID
     * @return java.util.List<com.atguigu.gulimall.product.entity.AttrGroupEntity>
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/04 14:55:57
     */
    List<AttrGroupEntity> listWithAttrByCatelogId(Long catelogId);
}

