package com.atguigu.gulimall.product.controller;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.R;
import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.entity.ProductAttrValueEntity;
import com.atguigu.gulimall.product.service.AttrService;
import com.atguigu.gulimall.product.service.ProductAttrValueService;
import com.atguigu.gulimall.product.vo.AttrRequestVo;
import com.atguigu.gulimall.product.vo.AttrResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 商品属性
 *
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 08:51:28
 */
@RefreshScope
@RestController
@RequestMapping("product/attr")
public class AttrController {
    @Autowired
    private AttrService attrService;
    @Autowired
    private ProductAttrValueService productAttrValueService;

    @Value("${product.common.brand}")
    private String brand;
    @Value("${product.base.price}")
    private int price;
    @Value("${product.base.cost}")
    private int cost;

    /**
     * 获取商品通用属性
     * @return com.atguigu.gulimall.common.utils.R 结果
     * @author lichao
     * @email li2chao@126.com
     * @date 2023/12/24 09:30:55
     */
    @RequestMapping("/common")
    public R getProductCommonAttr() {
        return R.ok().put("brand", brand)
                .put("price", price)
                .put("cost", cost);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("generator:attr:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = attrService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 获取商品属性列表
     * attrType = sale = 销售属性
     * attrType = base = 基本属性（规格参数）
     * attrType = both = 既是销售属性又是基本属性（规格参数）
     * @param catelogId 所属分类
     */
    @GetMapping("/{attrType}/list/{catelogId}")
    // @RequiresPermissions("generator:attr:list")
    public R listByCatelogId(
            @PathVariable("attrType") String attrType,
            @RequestParam Map<String, Object> params,
            @PathVariable("catelogId") Long catelogId) {
        PageUtils page = attrService.listByCatelogId(attrType, params, catelogId);

        return R.ok().put("page", page);
    }

    /**
     * 获取商品基本属性列表
     */
    @GetMapping("/base/listforspu/{spuId}")
    public R listBySpuId(@PathVariable("spuId") Long spuId) {
        List<ProductAttrValueEntity> data = productAttrValueService.listBySpuId(spuId);

        return R.ok().put("data", data);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrId}")
    // @RequiresPermissions("generator:attr:info")
    public R info(@PathVariable("attrId") Long attrId) {
        // AttrEntity attr = attrService.getById(attrId);
        AttrResponseVo attrResponseVo = attrService.getDetailById(attrId);

        return R.ok().put("attr", attrResponseVo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("generator:attr:save")
    public R save(@RequestBody AttrRequestVo attr) {
        // attrService.save(attr);
        /**
         * 级联保存
         * 1. 保存基本属性信息
         * 2. 如果不是销售属性，保存 [属性-属性分组-关联表] 信息
         */
        attrService.saveCascade(attr);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("generator:attr:update")
    public R update(@RequestBody AttrRequestVo attr) {
        // attrService.updateById(attr);
        /**
         * 级联更新
         * 1. 更新基本属性信息
         * 2. 如果不是销售属性，更新 [属性-属性分组-关联表] 信息
         */
        attrService.updateCascade(attr);

        return R.ok();
    }

    /**
     * 使用 SPU 标识进行批量修改
     */
    @RequestMapping("/update/{spuId}")
    public R updateBatchBySpuId(
            @PathVariable("spuId") Long spuId,
            @RequestBody List<ProductAttrValueEntity> productAttrValues) {
        productAttrValueService.updateBatchBySpuId(spuId, productAttrValues);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("generator:attr:delete")
    public R delete(@RequestBody Long[] attrIds) {
        // attrService.removeByIds(Arrays.asList(attrIds));
        /**
         * 级联删除
         * 1. 删除属性信息
         * 2. 如果不是销售属性，删除 [属性-属性分组-关联表] 信息
         */
        attrService.removeCascadeByIds(Arrays.asList(attrIds));

        return R.ok();
    }

}
