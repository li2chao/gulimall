package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @Name AttrResponseVo
 * @Description: 商品基本属性（规格参数）响应值对象
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-03 18:50:34
 **/
@Data
public class AttrResponseVo extends AttrVo {

    /**
     * 所属分类名称
     */
    private String catelogName;
    /**
     * 所属分类的完整 ID 路径
     * 例如: [2, 26, 235]
     */
    private Long[] catelogPath;
    /**
     * 所属分组 ID
     */
    private Long attrGroupId;
    /**
     * 所属分组名称
     */
    private String attrGroupName;

}

