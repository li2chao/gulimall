package com.atguigu.gulimall.product.feign;

import com.atguigu.gulimall.common.dto.SkuFullReductionDto;
import com.atguigu.gulimall.common.dto.SkuLadderDto;
import com.atguigu.gulimall.common.dto.SkuMemberPriceDto;
import com.atguigu.gulimall.common.dto.SpuBoundsDto;
import com.atguigu.gulimall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @Name CouponFeignService
 * @Description: 会员系统 - 远程调用服务
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-06 12:08:59
 **/
@FeignClient("gulimall-coupon")
public interface CouponFeignService {

    /**
     * 保存 SPU 会员积分信息
     *
     * @param spuBoundsDto SPU 会员积分数据传输对象
     * @return com.atguigu.gulimall.common.utils.R 结果
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/06 12:11:30
     */
    @PostMapping("/coupon/spubounds/save")
    R saveSpuBounds(@RequestBody SpuBoundsDto spuBoundsDto);

    /**
     * 保存 SKU 打折信息
     * @param skuLadderDto SKU 打折信息数据传输对象
     * @return com.atguigu.gulimall.common.utils.R 结果
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/06 12:53:35
     */
    @PostMapping("/coupon/skuladder/save")
    R saveSkuLadder(@RequestBody SkuLadderDto skuLadderDto);

    /**
     * 保存 SKU 满减信息
     * @param fullReductionDto SKU 打折信息数据传输对象
     * @return com.atguigu.gulimall.common.utils.R 结果
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/06 12:59:33
     */
    @PostMapping("/coupon/skufullreduction/save")
    R saveSkuFullReduction(SkuFullReductionDto fullReductionDto);

    /**
     * 批量保存 SKU 会员价格信息
     * @param memberPriceDtos SKU 会员价格数据传输对象
     * @return com.atguigu.gulimall.common.utils.R 结果
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/06 13:08:49
     */
    @PostMapping("/coupon/memberprice/saveBatch")
    R saveBatchSkuMemberPrice(List<SkuMemberPriceDto> memberPriceDtos);
}
