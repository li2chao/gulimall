package com.atguigu.gulimall.product.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Name SpuInfoResponseVo
 * @Description: 商品 SPU 信息响应值对象
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-06 15:10:00
 **/
@Data
public class SpuInfoResponseVo {
    /**
     * 商品id
     */
    private Long id;
    /**
     * 商品名称
     */
    private String spuName;
    /**
     * 商品描述
     */
    private String spuDescription;
    /**
     * 所属分类id
     */
    private Long catalogId;
    /**
     * 所属分类名称
     */
    private String catelogName;
    /**
     * 品牌id
     */
    private Long brandId;
    /**
     * 所属品牌名称
     */
    private String brandName;
    /**
     *
     */
    private BigDecimal weight;
    /**
     * 上架状态[0 - 下架，1 - 上架]
     */
    private Integer publishStatus;
    /**
     *
     */
    private Date createTime;
    /**
     *
     */
    private Date updateTime;
}

