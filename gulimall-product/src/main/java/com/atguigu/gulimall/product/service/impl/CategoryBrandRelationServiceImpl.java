package com.atguigu.gulimall.product.service.impl;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.Query;
import com.atguigu.gulimall.product.dao.BrandDao;
import com.atguigu.gulimall.product.dao.CategoryBrandRelationDao;
import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.entity.CategoryBrandRelationEntity;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("categoryBrandRelationService")
public class CategoryBrandRelationServiceImpl extends ServiceImpl<CategoryBrandRelationDao, CategoryBrandRelationEntity> implements CategoryBrandRelationService {

    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private BrandDao brandDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryBrandRelationEntity> page = this.page(
                new Query<CategoryBrandRelationEntity>().getPage(params),
                new QueryWrapper<CategoryBrandRelationEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveDetail(CategoryBrandRelationEntity categoryBrandRelation) {
        /**
         * 获取并设置分类-品牌-关联表中的冗余字段
         * catelog_name: 分类名称
         * brand_name: 品牌名称
         */
        Long catelogId = categoryBrandRelation.getCatelogId();
        CategoryEntity category = categoryDao.selectById(catelogId);
        categoryBrandRelation.setCatelogName(category.getName());

        Long brandId = categoryBrandRelation.getBrandId();
        BrandEntity brand = brandDao.selectById(brandId);
        categoryBrandRelation.setBrandName(brand.getName());

        this.save(categoryBrandRelation);
    }

    @Override
    public void updateBrandName(Long brandId, String brandName) {

        baseMapper.updateBrand(brandId, brandName);
    }

    @Override
    public void updateCategoryName(Long catId, String catName) {

        baseMapper.updateCategory(catId, catName);
    }

    @Override
    public List<CategoryBrandRelationEntity> listByCatelogId(Long catId) {
        List<CategoryBrandRelationEntity> result = baseMapper.selectList(
                new QueryWrapper<CategoryBrandRelationEntity>().eq("catelog_id", catId));

        return result;
    }

}