package com.atguigu.gulimall.product.exception;

import com.atguigu.gulimall.common.exception.BizCodeEnume;
import com.atguigu.gulimall.common.utils.R;
import jakarta.validation.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * @Name GulimallExceptionControllerAdvice
 * @Description: 商品服务 Controller 层接口异常处理
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2023-12-31 17:14:11
 **/
@Slf4j
@RestControllerAdvice(basePackages = "com.atguigu.gulimall.product.controller")
public class GulimallExceptionControllerAdvice {

    /**
     * JSR303 数据校验异常处理
     * @param e 捕获到的异常
     * @return com.atguigu.gulimall.common.utils.R 错误信息
     * @author lichao
     * @email li2chao@126.com
     * @date 2023/12/31 17:20:22
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R handleJSR303ValidException(MethodArgumentNotValidException e) {
        log.error("数据校验异常 -> {} 异常类型为: {}", e.getMessage(), e.getClass());

        Map<String, String> errorInfo = new HashMap<>();
        BindingResult result = e.getBindingResult();
        result.getFieldErrors().forEach((item) -> {
            String field = item.getField();
            String message = item.getDefaultMessage();
            errorInfo.put(field, message);
        });

        return R.error(BizCodeEnume.VALID_EXCEPTION.getCode(), BizCodeEnume.VALID_EXCEPTION.getMsg()).put("data", errorInfo);
    }

    /**
     * 缺省异常处理
     * @param throwable 捕获到的异常
     * @return com.atguigu.gulimall.common.utils.R 错误信息
     * @author lichao
     * @email li2chao@126.com
     * @date 2023/12/31 17:48:17
     */
    @ExceptionHandler(value = Throwable.class)
    public R defaultException(Throwable throwable) {
        log.error("系统未知异常 -> {} 异常类型为: {}", throwable.getMessage(), throwable.getClass());

        // TODO 缺省异常处理
        // ... ..

        return R.error(BizCodeEnume.UNKNOW_EXCEPTION.getCode(), BizCodeEnume.UNKNOW_EXCEPTION.getMsg());
    }

}

