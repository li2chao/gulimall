package com.atguigu.gulimall.product.entity;

import com.atguigu.gulimall.common.validator.ListValue;
import com.atguigu.gulimall.common.validator.group.AddGroup;
import com.atguigu.gulimall.common.validator.group.Group;
import com.atguigu.gulimall.common.validator.group.UpdateGroup;
import com.atguigu.gulimall.common.validator.group.UpdateStatusGroup;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.URL;

import java.io.Serializable;

/**
 * 品牌
 * 知识点：
 * [JSR303 规范]
 *   1. 添加依赖
 *   	[spring-boot-starter-validation]
 *   2. 给 Java Bean 中指定的字段添加校验注解（单一校验、分组校验、自定义校验注解等），并设置错误提示消息
 *   	2.1 单一校验
 *   		@Null、@NotNull、@NotEmpty、@NotBlank
 *			@URL、@Email、@Pattern
 * 			@Min、@Max
 * 			... ...
 *   	2.2 分组校验
 *   		背景：每个字段在面对不同的功能需求（新增、更新等）时可能需要满足多种校验规则，我们可以使用分组标注接口进行区分。
 *			实现：
 *				1）定义分组（标注）接口
 *				2）在校验注解中指定分组标注接口
 *				3）开启分组校验功能
 *   	2.3 自定义校验注解
 *   		背景：为满足个性化的校验规则，我们可以定义自己的校验注解进行实现。
 *   		实现：
 *   			1）定义自己的校验注解
 *   			2）实现自己的校验逻辑（器）
 *   			3）将{多个}校验逻辑（器）关联到校验注解，使其生效。
 *   3. 在 Controller 接口上开启 Java Bean 参数的校验功能
 *   	3.1 使用 @Valid 开启单一校验功能
 *   	3.2 使用 @Validated({分组标注接口.class}) 开启分组校验功能
 *   		注意：一旦开启了分组校验功能，所有的校验注解必须指定所属标注分组，未指定标注分组的校验注解默认不起作用。
 *   4. 异常处理
 *   	4.1 使用 BindingResult 对象在每个进行校验的 Java Bean 后面获取校验结果并逐一处理
 *   	4.2 使用 @RestControllerAdvice = @ControllerAdvice + @ExceptionHandler + @ResponseBody 注解实现统一的异常捕获与处理
 *   		补充说明：@ControllerAdvice + @ExceptionHandler 注解可以用于捕获并处理指定路径下的所有异常（不限于 JSR303 校验异常）。
 * [遇到的错误]
 *   1. 注解不生效
 *   	解决办法：所有注解均需导入 Hibernates 包的实现
 *   		[javax.validation.constraints] -> [jakarta.validation.constraints]
 * [参考文档]
 *   1. 官方文档
 *   2. https://blog.csdn.net/weixin_51751186/article/details/124739709
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 08:51:28
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
	@Null(message = "新增品牌信息不能指定品牌ID", groups = AddGroup.class)
	@NotNull(message = "修改品牌信息必须指定品牌ID", groups = UpdateGroup.class)
	@TableId
	private Long brandId;
	/**
	 * 品牌名
	 */
	@NotBlank(groups = {AddGroup.class, UpdateGroup.class})
	private String name;
	/**
	 * 品牌logo地址
	 */
	@NotBlank(groups = AddGroup.class)
	@URL(message = "LOGO 必须是一个合法的 URL 地址", groups = {AddGroup.class, UpdateGroup.class})
	private String logo;
	/**
	 * 介绍
	 */
	private String descript;
	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	@NotNull(groups = {AddGroup.class, UpdateStatusGroup.class})
	@ListValue(values = {0,1}, groups = {AddGroup.class, UpdateStatusGroup.class})
	private Integer showStatus;
	/**
	 * 检索首字母
	 */
	@NotBlank(groups = AddGroup.class)
	@Pattern(regexp = "^[a-zA-Z]$", message = "检索首字母必须是一个 [a-z | A-Z] 的合法字母", groups = {AddGroup.class, UpdateGroup.class})
	private String firstLetter;
	/**
	 * 排序
	 */
	@NotEmpty(groups = AddGroup.class)
	@Min(value = 0, message = "排序值必须是一个大于等于 0 的整数", groups = {AddGroup.class, UpdateGroup.class})
	private Integer sort;

}
