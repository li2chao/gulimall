package com.atguigu.gulimall.product.service.impl;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.Query;
import com.atguigu.gulimall.product.config.ProductConstants;
import com.atguigu.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.atguigu.gulimall.product.dao.AttrDao;
import com.atguigu.gulimall.product.dao.AttrGroupDao;
import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.entity.AttrGroupEntity;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.AttrService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.AttrRequestVo;
import com.atguigu.gulimall.product.vo.AttrResponseVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("attrService")
public class AttrServiceImpl extends ServiceImpl<AttrDao, AttrEntity> implements AttrService {

    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AttrAttrgroupRelationDao attrAttrgroupRelationDao;
    @Autowired
    private AttrGroupDao attrGroupDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                new QueryWrapper<AttrEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils listByCatelogId(String attrType, Map<String, Object> params, Long catelogId) {
        IPage<AttrEntity> page = null;
        QueryWrapper<AttrEntity> wrapper = new QueryWrapper<>();
        // 根据属性类型进行查询
        switch (attrType) {
            case ProductConstants.ATTR_TYPE_SALE_NAME:
                wrapper.eq("attr_type", 0);
                break;
            case ProductConstants.ATTR_TYPE_BASE_NAME:
                wrapper.eq("attr_type", 1);
                break;
            case ProductConstants.ATTR_TYPE_BOTH_NAME:
                wrapper.eq("attr_type", 2);
                break;
            default:
                wrapper.eq("attr_type", 1);
        }
        // 支持商品属性的多字段模糊查询
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            wrapper.eq("attr_id", key).or().like("attr_name", key);
        }
        // 获取指定分类下的商品属性
        if (catelogId != 0) {
            wrapper.eq("catelog_id", catelogId);
        }
        page = this.page(new Query<AttrEntity>().getPage(params), wrapper);

        // 设置属性的扩展信息: 所属分类名称、基本属性的所属分组名称
        List<AttrEntity> attrEntities = page.getRecords();
        List<AttrResponseVo> attrResponseVoList = attrEntities.stream().map((attrEntity) -> {
            AttrResponseVo attrResponseVo = new AttrResponseVo();
            BeanUtils.copyProperties(attrEntity, attrResponseVo);
            // 设置所属分类名称
            CategoryEntity category = categoryDao.selectById(attrEntity.getCatelogId());
            if (category != null) {
                attrResponseVo.setCatelogName(category.getName());
            }
            // 为非销售属性设置所属分组名称
            if (!StringUtils.equals(attrType, ProductConstants.ATTR_TYPE_SALE_NAME)) {
                AttrAttrgroupRelationEntity attrAttrgroupRelation = attrAttrgroupRelationDao.selectOne(
                        new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attrEntity.getAttrId()));
                if (attrAttrgroupRelation != null) {
                    AttrGroupEntity attrGroup = attrGroupDao.selectById(attrAttrgroupRelation.getAttrGroupId());
                    attrResponseVo.setAttrGroupName(attrGroup.getAttrGroupName());
                }
            }
            return attrResponseVo;
        }).collect(Collectors.toList());

        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(attrResponseVoList);
        return pageUtils;
    }

    @Override
    public AttrResponseVo getDetailById(Long attrId) {
        AttrEntity attr = this.getById(attrId);
        AttrResponseVo attrResponseVo = new AttrResponseVo();
        BeanUtils.copyProperties(attr, attrResponseVo);
        // 设置所属分类的完整 ID 路径
        if (attr.getCatelogId() != null) {
            Long[] categoryPath = categoryService.findCategoryPath(attr.getCatelogId());
            attrResponseVo.setCatelogPath(categoryPath);
            // 补充设置所属分类的名称
            CategoryEntity category = categoryDao.selectById(attr.getCatelogId());
            attrResponseVo.setCatelogName(category.getName());
        }
        // 为非销售属性设置所属分组 ID
        if (attr.getAttrType() != ProductConstants.ATTR_TYPE_SALE_CODE) {
            AttrAttrgroupRelationEntity attrAttrgroupRelation = attrAttrgroupRelationDao.selectOne(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attrId));
            if (attrAttrgroupRelation != null) {
                attrResponseVo.setAttrGroupId(attrAttrgroupRelation.getAttrGroupId());
                // 补充设置所属分组的名称
                AttrGroupEntity attrGroup = attrGroupDao.selectById(attrAttrgroupRelation.getAttrGroupId());
                attrResponseVo.setAttrGroupName(attrGroup.getAttrGroupName());
            }
        }

        return attrResponseVo;
    }

    @Transactional
    @Override
    public void saveCascade(AttrRequestVo attr) {
        // 保存基本属性信息
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr, attrEntity);
        baseMapper.insert(attrEntity);

        // 为非销售属性保存 [属性-属性分组-关联表] 信息
        if (attr.getAttrType() != ProductConstants.ATTR_TYPE_SALE_CODE && attr.getAttrGroupId() != null) {
            AttrAttrgroupRelationEntity attrAttrgroupRelation = new AttrAttrgroupRelationEntity();
            attrAttrgroupRelation.setAttrId(attr.getAttrId());
            attrAttrgroupRelation.setAttrGroupId(attr.getAttrGroupId());
            attrAttrgroupRelationDao.insert(attrAttrgroupRelation);
        }
    }

    @Transactional
    @Override
    public void updateCascade(AttrRequestVo attr) {
        // 更新基本属性信息
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr, attrEntity);
        baseMapper.updateById(attrEntity);

        // 为非销售属性更新 [属性-属性分组-关联表] 信息
        if (attrEntity.getAttrType() != ProductConstants.ATTR_TYPE_SALE_CODE) {
            AttrAttrgroupRelationEntity attrAttrgroupRelation = new AttrAttrgroupRelationEntity();
            attrAttrgroupRelation.setAttrId(attr.getAttrId());
            attrAttrgroupRelation.setAttrGroupId(attr.getAttrGroupId());
            long count = attrAttrgroupRelationDao.selectCount(
                    new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attr.getAttrId()));
            if (count > 0) {
                // 如果 [属性-属性分组-关联表] 信息存在，则更新
                attrAttrgroupRelationDao.update(attrAttrgroupRelation,
                        new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attr.getAttrId()));
            } else {
                // 如果 [属性-属性分组-关联表] 信息存不在，则新增
                attrAttrgroupRelationDao.insert(attrAttrgroupRelation);
            }
        }
    }

    @Transactional
    @Override
    public void removeCascadeByIds(List<Long> attrIds) {
        // 删除属性基本信息
        baseMapper.deleteBatchIds(attrIds);

        // 如果不是销售属性，删除 [属性-属性分组-关联表] 信息
        attrIds.stream().forEach(id -> {
            AttrEntity attr = this.getById(id);
            if (attr.getAttrType() != ProductConstants.ATTR_TYPE_SALE_CODE) {
                this.removeById(id);
            }
        });
    }

    @Override
    public List<AttrEntity> listByAttrGroupId(Long attrgroupId) {
        List<AttrAttrgroupRelationEntity> attrAttrgroupRelationList = attrAttrgroupRelationDao.selectList(
                new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_group_id", attrgroupId)
        );

        List<Long> attrIds = attrAttrgroupRelationList.stream()
                .map(relation -> relation.getAttrId())
                .collect(Collectors.toList());

        List<AttrEntity> result = null;
        if (attrIds != null && attrIds.size() > 0) {
            result = this.listByIds(attrIds);
        }
        return result;
    }

    @Override
    public PageUtils listBaseAttrByNoAttrGroupId(Map<String, Object> params, Long attrgroupId) {
        // 1. 获取 [属性分组-属性-关联表] 信息
        List<AttrAttrgroupRelationEntity> attrAttrgroupRelationList = attrAttrgroupRelationDao.selectList(
                new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_group_id", attrgroupId));

        // 2. 获取所有分组里面还没有关联的其他基本属性
        QueryWrapper<AttrEntity> queryWrapper = new QueryWrapper<AttrEntity>().eq("attr_type", ProductConstants.ATTR_TYPE_BASE_CODE);
        attrAttrgroupRelationList.stream().forEach(relation -> queryWrapper.ne("attr_id", relation.getAttrId()));

        // 3. 查询并返回分页结果, 支持多字段模糊查询
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            queryWrapper.eq("attr_id", key).or().like("attr_name", key);
        }
        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params), queryWrapper);
        return new PageUtils(page);
    }

}