package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @Name AttrRequestVo
 * @Description: 商品基本属性（规格参数）请求值对象
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-03 19:37:15
 **/
@Data
public class AttrRequestVo extends AttrVo {

    /**
     * 所属分组 ID
     */
    private Long attrGroupId;

}

