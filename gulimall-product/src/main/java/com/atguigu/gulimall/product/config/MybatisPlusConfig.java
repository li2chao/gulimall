package com.atguigu.gulimall.product.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Name MybatisPlusConfig
 * @Description: MybatisPlus 组件配置类
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-02 17:35:38
 **/
@Configuration
// 开启事务管理
@EnableTransactionManagement
@MapperScan("com.atguigu.gulimall.product.dao")
public class MybatisPlusConfig {

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();

        // 添加分页插件
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        // 1. 设置数据库类型
        // 注意: 如果有多数据源可以不配具体类型，否则都建议配上具体的 DbType。
        paginationInnerInterceptor.setDbType(DbType.MYSQL);
        // 2. 设置请求的页面大于最大页后如何操作
        // true: 返回首页
        // false: 默认值，继续发起请求
        paginationInnerInterceptor.setOverflow(true);
        // 3. 设置最大单页限制数量，默认为 500 条，-1 则不受限制
        paginationInnerInterceptor.setMaxLimit(1000L);
        interceptor.addInnerInterceptor(paginationInnerInterceptor);

        // 如果配置多个插件,则在分页插件后面继续添加
        // ... ...
        return interceptor;
    }
}

