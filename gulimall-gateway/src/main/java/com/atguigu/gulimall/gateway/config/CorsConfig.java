package com.atguigu.gulimall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.server.ServerWebExchange;

/**
 * @Name CorsConfig
 * @Description: 跨域配置
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2023-12-27 15:56:04
 **/
@Configuration
public class CorsConfig {

    @Bean
    public CorsWebFilter corsWebFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        CorsConfiguration corsConfiguration = new CorsConfiguration();
        // 1. 跨域配置
        // 允许所有的请求来源 {即放行所有请求来源 http|https://domain|ip:port} 进行跨域
        corsConfiguration.addAllowedOriginPattern("*");
        // 允许所有的请求头 {放行所有的请求头信息} 进行跨域
        corsConfiguration.addAllowedHeader("*");
        // 允许所有的请求方式 {GET、POST、DELETE ... ...} 进行跨域
        corsConfiguration.addAllowedMethod("*");
        // 允许所有的 Cookie {放行所有的客户端会话信息} 进行跨域
        corsConfiguration.setAllowCredentials(true);

        source.registerCorsConfiguration("/**", corsConfiguration);

        return new CorsWebFilter(source);
    }

}

