package com.atguigu.gulimall.thirdparty;

import com.atguigu.gulimall.thirdparty.utils.AliyunOSSUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallThirdPartyApplicationTests {

	@Autowired
	AliyunOSSUtils aliyunOSSUtils;

	@Test
	public void contextLoads() throws Exception {
		String path = "C:\\Users\\zawm\\Pictures\\截图20211221103726.png";

		String url = aliyunOSSUtils.upload(path);

		System.out.println("上传成功, OSS 文件访问路径为: " + url);
	}

}
