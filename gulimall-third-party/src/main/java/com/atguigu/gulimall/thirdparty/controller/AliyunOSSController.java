package com.atguigu.gulimall.thirdparty.controller;

import com.atguigu.gulimall.common.utils.R;
import com.atguigu.gulimall.thirdparty.utils.AliyunOSSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Name AliyunOSSController
 * @Description: 阿里云 OSS 服务接口
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2023-12-31 14:43:52
 **/
@RestController
@RequestMapping("/aliyun/oss")
public class AliyunOSSController {

    @Autowired
    private AliyunOSSUtils ossUtils;

    /**
     * 获取 OSS 服务端签名值及上传策略信息
     * @return com.atguigu.gulimall.common.utils.R
     * @author lichao
     * @email li2chao@126.com
     * @date 2023/12/31 15:08:05
     */
    @RequestMapping("/policy")
    public R policy() {
        Map<String, String> policy = ossUtils.genOssPolicy();

        return R.ok().put("data", policy);
    }

}

