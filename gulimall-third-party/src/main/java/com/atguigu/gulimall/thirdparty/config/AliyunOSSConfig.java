package com.atguigu.gulimall.thirdparty.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Name AliyunOSSConfig
 * @Description: 阿里云 OSS 配置
 * [补充说明]
 *   正常情况下，不需要使用此配置类去读取 OSS 的配置手动实例化 OSSClient 对象，而是由 spring-cloud-starter-alicloud-oss 组件自动化的完成此项工作。但是却遇到了以下问题：
 *   Caused by: org.springframework.beans.factory.NoSuchBeanDefinitionException: No qualifying bean of type 'com.aliyun.oss.OSS' available.
 *   且翻阅了大量文档，浪费了很多时间仍没有解决。
 *   为了节省时间，先采用以下方式进行实现。
 *   后续如果有精力，再去解决。
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2023-12-31 11:34:37
 **/
@Configuration
public class AliyunOSSConfig implements InitializingBean {

    public static String HOST;
    public static String ENDPOINT;
    public static String ACCESS_KEY;
    public static String SECRET_KEY;
    public static String BUCKET;

    @Value("${spring.cloud.alicloud.access-key}")
    private String accessKey;

    @Value("${spring.cloud.alicloud.secret-key}")
    private String secretKey;

    @Value("${spring.cloud.alicloud.oss.endpoint}")
    private String endpoint;

    @Value("${spring.cloud.alicloud.oss.bucket}")
    private String bucket;

    @Override
    public void afterPropertiesSet() throws Exception {
        HOST = "https://" + bucket + "." + endpoint;
        ENDPOINT = endpoint;
        ACCESS_KEY = accessKey;
        SECRET_KEY = secretKey;
        BUCKET = bucket;
    }

    @Bean
    public OSS INSTANCE() {
        return new OSSClient(endpoint, accessKey, secretKey);
    }

}

