package com.atguigu.gulimall.thirdparty.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.model.MatchMode;
import com.aliyun.oss.model.PolicyConditions;
import com.atguigu.gulimall.thirdparty.config.AliyunOSSConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Name AliyunOSSUtils
 * @Description: 阿里云 OSS 工具类
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2023-12-31 11:46:53
 **/
@Component
public class AliyunOSSUtils {

    @Autowired
    private OSS ossClient;

    /**
     * 服务端签名直传服务
     * @return java.util.Map<java.lang.String, java.lang.String> 签名信息
     * @author lichao
     * @email li2chao@126.com
     * @date 2023/12/31 14:40:19
     */
    public Map<String, String> genOssPolicy() {
        // 设置上传到OSS文件的前缀，可置空此项。置空后，文件将上传至Bucket的根目录下。
        String dir = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "/";
        Map<String, String> respMap = null;
        try {
            long expireTime = 30;
            long expireEndTime = System.currentTimeMillis() + expireTime * 1000;
            Date expiration = new Date(expireEndTime);
            // PostObject 请求最大可支持的文件大小为 5 GB，即 CONTENT_LENGTH_RANGE 为 5*1024*1024*1024。
            PolicyConditions policyConds = new PolicyConditions();
            policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, 1048576000);
            policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, dir);

            String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
            byte[] binaryData = postPolicy.getBytes("utf-8");
            String encodedPolicy = BinaryUtil.toBase64String(binaryData);
            String postSignature = ossClient.calculatePostSignature(postPolicy);

            respMap = new LinkedHashMap<String, String>();
            respMap.put("accessid", AliyunOSSConfig.ACCESS_KEY);
            respMap.put("policy", encodedPolicy);
            respMap.put("signature", postSignature);
            respMap.put("dir", dir);
            respMap.put("host", AliyunOSSConfig.HOST);
            respMap.put("expire", String.valueOf(expireEndTime / 1000));
        } catch (Exception e) {
            // Assert.fail(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            ossClient.shutdown();
        }
        return respMap;
    }

    /**
     * 文件上传
     * @param file 多媒体文件
     * @return java.lang.String
     * @author lichao
     * @email li2chao@126.com
     * @date 2023/12/31 12:01:18
     */
    public String upload(MultipartFile file) throws IOException {
        // 生成 OSS 唯一文件名称，避免文件覆盖
        String fileName = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss")) + "-" + file.getOriginalFilename();

        // 上传文件至 OSS
        InputStream in = file.getInputStream();
        ossClient.putObject(AliyunOSSConfig.BUCKET, fileName, in);

        // 获取 OSS 文件访问路径
        String url = AliyunOSSConfig.HOST + "/" + fileName;

        // 关闭 OSS 客户端
        ossClient.shutdown();

        return url;
    }

    /**
     * 文件上传
     * @param path 文件路径
     * @return java.lang.String
     * @author lichao
     * @email li2chao@126.com
     * @date 2023/12/31 12:01:36
     */
    public String upload(String path) throws IOException {
        // 生成 OSS 唯一文件名称，避免文件覆盖
        File file = new File(path);
        String fileName = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss")) + "-" + file.getName();

        // 上传文件至 OSS
        InputStream in = new FileInputStream(path);
        ossClient.putObject(AliyunOSSConfig.BUCKET, fileName, in);

        // 获取 OSS 文件访问路径
        String url = AliyunOSSConfig.HOST + "/" + fileName;

        // 关闭 OSS 客户端
        ossClient.shutdown();

        return url;
    }


}

