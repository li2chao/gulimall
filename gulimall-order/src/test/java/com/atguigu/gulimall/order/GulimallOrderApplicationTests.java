package com.atguigu.gulimall.order;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.atguigu.gulimall.order.service.OrderService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class GulimallOrderApplicationTests {

    @Autowired
    OrderService orderService;

    @Test
    void contextLoads() {
        //OrderEntity orderEntity = new OrderEntity();
        //orderEntity.setMemberUsername("张三");
        //orderService.save(orderEntity);
        //System.out.println("保存成功");
        OrderEntity orderEntity = orderService.getById(1L);
        System.out.println(orderEntity);
    }

}
