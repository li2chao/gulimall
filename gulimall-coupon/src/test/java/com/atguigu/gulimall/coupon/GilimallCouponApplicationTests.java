package com.atguigu.gulimall.coupon;

import com.atguigu.gulimall.coupon.entity.CouponEntity;
import com.atguigu.gulimall.coupon.service.CouponService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class GilimallCouponApplicationTests {

    @Autowired
    CouponService couponService;

    @Test
    void contextLoads() {
        //CouponEntity couponEntity = new CouponEntity();
        //couponEntity.setCouponName("全场 9 折优惠券");
        //couponService.save(couponEntity);
        //System.out.println("保存成功");
        CouponEntity couponEntity = couponService.getById(1L);
        System.out.println(couponEntity);
    }

}
