package com.atguigu.gulimall.coupon.service;

import com.atguigu.gulimall.common.dto.SkuLadderDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.coupon.entity.SkuLadderEntity;

import java.util.Map;

/**
 * 商品阶梯价格
 *
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 15:43:52
 */
public interface SkuLadderService extends IService<SkuLadderEntity> {

    PageUtils queryPage(Map<String, Object> params);

}

