package com.atguigu.gulimall.common.validator.group;

/**
 * @Name 更新状态 Group
 * @Description: 更新状态校验分组标注接口
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2023-12-31 21:22:03
 **/
public interface UpdateStatusGroup {
}
