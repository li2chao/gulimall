package com.atguigu.gulimall.common.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

/**
 * @Name ListValue
 * @Description: 自定义校验注解
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2023-12-31 19:55:00
 **/
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        validatedBy = { ListValueConstraintValidator.class }
)
public @interface ListValue {

    String message() default "{com.atguigu.gulimall.common.validator.ListValue.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int[] values() default {};

}
