/**
 * Copyright 2019 bejson.com
 */
package com.atguigu.gulimall.common.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Name SkuMemberPriceDto
 * @Description: 数据传输对象 - 会员价格信息
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-06 12:37:20
 **/
@Data
public class SkuMemberPriceDto {
    /**
     * 商品 SKU 标识
     */
    private Long skuId;
    /**
     * 会员等级 ID
     */
    private Long memberLevelId;
    /**
     * 会员等级名
     */
    private String memberLevelName;
    /**
     * 会员对应价格
     */
    private BigDecimal membePrice;
}