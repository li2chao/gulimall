package com.atguigu.gulimall.common.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Name SkuFullReductionDto
 * @Description: 数据传输对象 - 商品 SKU 满减信息
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-06 12:41:40
 **/
@Data
public class SkuFullReductionDto {
    /**
     * 商品 SKU 标识
     */
    private Long skuId;
    /**
     * 满多少
     */
    private BigDecimal fullPrice;
    /**
     * 减多少
     */
    private BigDecimal reducePrice;
    /**
     * 是否参与其他优惠
     */
    private int addOther;
}

