package com.atguigu.gulimall.common.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Name SkuLadderDto
 * @Description: 数据传输对象 - 商品 SKU 打折信息
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-06 12:37:20
 **/
@Data
public class SkuLadderDto {
    /**
     * 商品 SKU 标识
     */
    private Long skuId;
    /**
     * 满几件
     */
    private int fullCount;
    /**
     * 打几折
     */
    private BigDecimal discount;
    /**
     * 是否叠加其他优惠[0-不可叠加，1-可叠加]
     */
    private int addOther;


}

