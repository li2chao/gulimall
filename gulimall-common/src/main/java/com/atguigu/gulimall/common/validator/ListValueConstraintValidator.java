package com.atguigu.gulimall.common.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.io.OptionalDataException;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @Name ListValueConstraintValidator
 * @Description: 自定义校验注解 ListValue 的校验逻辑（器）
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2023-12-31 20:06:03
 **/
public class ListValueConstraintValidator implements ConstraintValidator<ListValue, Integer> {

    private Set<Integer> values = new HashSet<>();

    /**
     * 初始化方法：获取 ListValue 校验注解中指定的 values 内容
     * @param listValue 校验注解对象
     * @author lichao
     * @email li2chao@126.com
     * @date 2023/12/31 20:11:16
     */
    @Override
    public void initialize(ListValue listValue) {
        int[] items = listValue.values();
        for (int item: items) {
            values.add(item);
        }
    }

    /**
     * 校验逻辑：判断参数内容是否符合自定义的校验规则
     * @param value 参数内容
     * @param constraintValidatorContext 校验器上下文
     * @return boolean
     * @author lichao
     * @email li2chao@126.com
     * @date 2023/12/31 20:23:04
     */
    @Override
    public boolean isValid(final Integer value, ConstraintValidatorContext constraintValidatorContext) {
        if (values.contains(value)) {
            return true;
        }
        return false;
    }
}

