package com.atguigu.gulimall.common.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Name SpuBoundsDto
 * @Description: 数据传输对象 - 会员积分信息
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-06 11:40:21
 **/
@Data
public class SpuBoundsDto {

    /**
     * spu 标识
     */
    private Long spuId;
    /**
     * 购物积分
     */
    private BigDecimal buyBounds;
    /**
     * 成长积分
     */
    private BigDecimal growBounds;

}

