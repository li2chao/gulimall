package com.atguigu.gulimall.ware.config;

/**
 * @Name PurchaseDetailStatusEnum
 * @Description: 采购需求状态枚举类
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-06 20:06:49
 **/
public enum PurchaseDetailStatusEnum {
    PURCHASE_DETAIL_STATUS_CREATED(0, "已创建"),
    PURCHASE_DETAIL_STATUS_ASSIGNED(1, "已分配"),
    PURCHASE_DETAIL_STATUS_BUYING(2, "采购中"),
    PURCHASE_DETAIL_STATUS_COMPLETED(3, "已完成"),
    PURCHASE_DETAIL_STATUS_FAILED(4, "采购失败");


    private int code;
    private String msg;

    PurchaseDetailStatusEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
