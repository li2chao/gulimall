package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @Name PurchaseDoneVo
 * @Description: 完成采购单值对象
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-06 21:51:48
 **/
@Data
public class PurchaseDoneVo {
    /**
     * 完成采购单 ID
     */
    private Long id;
    /**
     * 完成采购需求（项） 列表
     */
    private List<PurchaseDetailDoneVo> items;


}

