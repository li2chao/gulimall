package com.atguigu.gulimall.ware.service;

import com.atguigu.gulimall.ware.vo.PurchaseDetailMergeVo;
import com.atguigu.gulimall.ware.vo.PurchaseDoneVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.ware.entity.PurchaseEntity;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 15:40:28
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnreceivePurchase();

    /**
     * 合并采购需求至采购单
     * @param detailMergeVo 合并采购需求值对象
     * @author lichao
     * @email li2chao@126.com
     * @date 2024/01/06 19:33:37
     */
    void mergePurchaseDetails(PurchaseDetailMergeVo detailMergeVo);

    void receivedByIds(List<Long> purchaseIds);

    void purchaseDone(PurchaseDoneVo purchaseDoneVo);
}

