package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @Name PurchaseMergeVo
 * @Description: 采购需求合并值对象
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-06 19:52:05
 **/
@Data
public class PurchaseDetailMergeVo {
    /**
     * 采购单 ID
     */
    private Long purchaseId;
    /**
     * 采购需求 ID 集合
     */
    private List<Long> items;

}

