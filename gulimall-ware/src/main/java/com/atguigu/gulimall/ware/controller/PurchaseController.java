package com.atguigu.gulimall.ware.controller;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.R;
import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.atguigu.gulimall.ware.service.PurchaseService;
import com.atguigu.gulimall.ware.vo.PurchaseDetailMergeVo;
import com.atguigu.gulimall.ware.vo.PurchaseDoneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 * 采购信息
 *
 * @author lichao
 * @email li2chao@126.com
 * @date 2023-12-22 15:40:28
 */
@RestController
@RequestMapping("ware/purchase")
public class PurchaseController {
    @Autowired
    private PurchaseService purchaseService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("ware:purchase:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 未领取（已创建或已分配）的采购列表
     */
    @RequestMapping("/unreceive/list")
    public R unreceiveList() {
        PageUtils page = purchaseService.queryPageUnreceivePurchase();

        return R.ok().put("page", page);
    }

    /**
     * 采购人员领取采购单
     */
    @RequestMapping("/received")
    public R received(@RequestParam List<Long> purchaseIds) {
        purchaseService.receivedByIds(purchaseIds);

        return R.ok();
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("ware:purchase:info")
    public R info(@PathVariable("id") Long id){
		PurchaseEntity purchase = purchaseService.getById(id);

        return R.ok().put("purchase", purchase);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("ware:purchase:save")
    public R save(@RequestBody PurchaseEntity purchase){
		purchaseService.save(purchase);

        return R.ok();
    }

    /**
     * 合并采购需求
     */
    @RequestMapping("/merge")
    // @RequiresPermissions("ware:purchase:save")
    public R mergePurchaseDetail(@RequestBody PurchaseDetailMergeVo detailMergeVo) {
        purchaseService.mergePurchaseDetails(detailMergeVo);

        return R.ok();
    }

    /**
     * 完成采购
     */
    @RequestMapping("/done")
    public R purchaseDone(@RequestBody PurchaseDoneVo purchaseDoneVo) {
        purchaseService.purchaseDone(purchaseDoneVo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("ware:purchase:update")
    public R update(@RequestBody PurchaseEntity purchase){
		purchaseService.updateById(purchase);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("ware:purchase:delete")
    public R delete(@RequestBody Long[] ids){
		purchaseService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
