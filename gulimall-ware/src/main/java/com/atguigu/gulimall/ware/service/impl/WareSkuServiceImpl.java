package com.atguigu.gulimall.ware.service.impl;

import com.alibaba.nacos.shaded.com.google.gson.JsonObject;
import com.atguigu.gulimall.common.utils.R;
import com.atguigu.gulimall.ware.feign.ProductFeignService;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.util.MapUtil;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.Query;

import com.atguigu.gulimall.ware.dao.WareSkuDao;
import com.atguigu.gulimall.ware.entity.WareSkuEntity;
import com.atguigu.gulimall.ware.service.WareSkuService;


@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {

    @Autowired
    ProductFeignService productFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareSkuEntity> queryWrapper = new QueryWrapper<>();
        // 支持按照 SKU 标识进行查询
        String skuId = (String) params.get("skuId");
        if (StringUtils.isNotBlank(skuId) && !StringUtils.equals(skuId, "0")) {
            queryWrapper.eq("sku_id", skuId);
        }
        // 支持按照仓库标识进行查询
        String wareId = (String) params.get("wareId");
        if (StringUtils.isNotBlank(wareId) && !StringUtils.equals(wareId, "0")) {
            queryWrapper.eq("ware_id", wareId);
        }
        IPage<WareSkuEntity> page = this.page(new Query<WareSkuEntity>().getPage(params), queryWrapper);
        return new PageUtils(page);
    }

    @Override
    public void syncWareSku(Long skuId, Long wareId, Integer skuNum) {
        // 完成采购需求（项）之后，更新库存信息
        long count = this.count(
                new QueryWrapper<WareSkuEntity>().eq("sku_id", skuId).eq("ware_id", wareId)
        );
        if (count > 0) {
            // 1. 如果库存中存在采购需求（项），则更新库存数量
            baseMapper.updateStock(skuId, wareId, skuNum);
        } else {
            // 2. 否则，新增一条库存记录
            WareSkuEntity wareSku = new WareSkuEntity();
            wareSku.setSkuId(skuId);
            // 远程获取并设置 SKU 名称
            try {
                R result = productFeignService.getSkuInfo(skuId);
                int code = (int) result.get("code");
                if(code == 0) {
                    Map<String, Object> skuInfo = (Map<String, Object>) result.get("skuInfo");
                    wareSku.setSkuName((String) skuInfo.get("skuName"));
                }
            } catch (Exception e) {
                /**
                 * 出现异常后如何避免事务回滚？？？
                 * 方法一：
                 *     必须要在这里捕获并处理掉（或者什么也不做）异常，切记千万不要向上抛出异常。
                 *     因为在整个方法调用栈中，系统一旦检测到异常，将会进行整体事务回滚。
                 *     仅仅因为设置一个 SKU 名称字段而导致整个事务回滚，因小失大。
                 * 其他方法：
                 *     TODO 待补充。。。
                 */
            }
            wareSku.setWareId(wareId);
            wareSku.setStock(skuNum);
            wareSku.setStockLocked(0);
            baseMapper.insert(wareSku);
        }
    }

}