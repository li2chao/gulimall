package com.atguigu.gulimall.ware.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.Query;

import com.atguigu.gulimall.ware.dao.PurchaseDetailDao;
import com.atguigu.gulimall.ware.entity.PurchaseDetailEntity;
import com.atguigu.gulimall.ware.service.PurchaseDetailService;


@Service("purchaseDetailService")
public class PurchaseDetailServiceImpl extends ServiceImpl<PurchaseDetailDao, PurchaseDetailEntity> implements PurchaseDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<PurchaseDetailEntity> queryWrapper = new QueryWrapper<>();
        // 支持多字段联合查询
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            queryWrapper.and(wrapper -> {
                wrapper.eq("id", key)
                        .or().eq("purchase_id", key)
                        .or().eq("sku_id", key)
                        .or().eq("sku_num", key);
            });
        }
        // 支持按照仓库 ID 进行查询
        String wareId = (String) params.get("wareId");
        if (StringUtils.isNotBlank(wareId) && !StringUtils.equals(wareId, "0")) {
            queryWrapper.eq("ware_id", wareId);
        }
        // 支持按照状态进行查询
        String status = (String) params.get("status");
        if (StringUtils.isNotBlank(status) && !StringUtils.equals(status, "0")) {
            queryWrapper.eq("status", status);
        }
        IPage<PurchaseDetailEntity> page = this.page(new Query<PurchaseDetailEntity>().getPage(params), queryWrapper);
        return new PageUtils(page);
    }

    @Override
    public List<PurchaseDetailEntity> listByPurchaseId(Long id) {
        List<PurchaseDetailEntity> result = baseMapper.selectList(new QueryWrapper<PurchaseDetailEntity>().eq("purchase_id", id));

        return result;
    }

}