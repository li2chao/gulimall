package com.atguigu.gulimall.ware.service.impl;

import com.atguigu.gulimall.common.utils.PageUtils;
import com.atguigu.gulimall.common.utils.Query;
import com.atguigu.gulimall.ware.config.PurchaseDetailStatusEnum;
import com.atguigu.gulimall.ware.config.PurchaseStatusEnum;
import com.atguigu.gulimall.ware.dao.PurchaseDao;
import com.atguigu.gulimall.ware.entity.PurchaseDetailEntity;
import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.atguigu.gulimall.ware.service.PurchaseDetailService;
import com.atguigu.gulimall.ware.service.PurchaseService;
import com.atguigu.gulimall.ware.service.WareSkuService;
import com.atguigu.gulimall.ware.vo.PurchaseDetailDoneVo;
import com.atguigu.gulimall.ware.vo.PurchaseDetailMergeVo;
import com.atguigu.gulimall.ware.vo.PurchaseDoneVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


@Service("purchaseService")
public class PurchaseServiceImpl extends ServiceImpl<PurchaseDao, PurchaseEntity> implements PurchaseService {

    @Autowired
    private PurchaseDetailService purchaseDetailService;
    @Autowired
    private WareSkuService wareSkuService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageUnreceivePurchase() {
        QueryWrapper<PurchaseEntity> queryWrapper = new QueryWrapper<>();
        // 仅查询已创建或已分配的采购单信息
        queryWrapper.eq("status", PurchaseStatusEnum.PURCHASE_STATUS_CREATED.getCode())
                .or().eq("status", PurchaseStatusEnum.PURCHASE_STATUS_ASSIGNED.getCode());
        IPage<PurchaseEntity> page = this.page(new Query<PurchaseEntity>().getPage(null), queryWrapper);
        return new PageUtils(page);
    }

    @Override
    public void mergePurchaseDetails(PurchaseDetailMergeVo detailMergeVo) {
        /**
         * 将多个采购需求合并至采购单
         * 1. 如果采购单 ID 为空，则新建一个采购单
         * 2. 如果采购单 ID 不为空，则合并至目标（未领取的）采购单内，并重置采购单的更新时间
         * 3. 更新每个采购需求的采购单 ID 和状态
         */
        // 将多个采购需求合并至采购单
        Long purchaseId = detailMergeVo.getPurchaseId();
        // 如果采购单 ID 为空，则新建一个采购单
        if (purchaseId == null) {
            PurchaseEntity purchase = new PurchaseEntity();
            purchase.setStatus(PurchaseStatusEnum.PURCHASE_STATUS_CREATED.getCode());
            purchase.setCreateTime(new Date());
            purchase.setUpdateTime(new Date());
            baseMapper.insert(purchase);
        } else {
            // 如果采购单 ID 不为空，则合并至目标（未领取的）采购单内，并重置采购单的更新时间
            PurchaseEntity entity = baseMapper.selectById(purchaseId);
            if (entity.getStatus().intValue() == PurchaseStatusEnum.PURCHASE_STATUS_CREATED.getCode() || entity.getStatus().intValue() == PurchaseStatusEnum.PURCHASE_STATUS_ASSIGNED.getCode()) {
                entity.setUpdateTime(new Date());
                baseMapper.updateById(entity);
            }
        }
        // 更新每个采购需求的采购单 ID 和状态
        List<PurchaseDetailEntity> purchaseDetailEntities = detailMergeVo.getItems().stream()
                .map(item -> {
                    PurchaseDetailEntity purchaseDetail = new PurchaseDetailEntity();
                    purchaseDetail.setId(item);
                    purchaseDetail.setPurchaseId(purchaseId);
                    purchaseDetail.setStatus(PurchaseDetailStatusEnum.PURCHASE_DETAIL_STATUS_ASSIGNED.getCode());
                    return purchaseDetail;
                }).collect(Collectors.toList());
        purchaseDetailService.updateBatchById(purchaseDetailEntities);
    }

    @Transactional
    @Override
    public void receivedByIds(List<Long> purchaseIds) {
        /**
         * 由采购人员领取采购单
         * 1. 采购人员
         * 发送待领取的采购单 ID 列表
         * 2. 采购系统
         * 2.1 更新采购单的状态为已领取
         * 2.2 更新所有（状态为未采购的）采购需求（项）的状态为正在采购
         */
        Optional<List<Long>> purchaseIdsOptional = Optional.ofNullable(purchaseIds);
        purchaseIdsOptional.ifPresent(ids -> {
            // 更新采购单的状态为已领取
            List<PurchaseEntity> purchaseEntities = ids.stream().map(id -> {
                PurchaseEntity purchase = new PurchaseEntity();
                purchase.setId(id);
                purchase.setStatus(PurchaseStatusEnum.PURCHASE_STATUS_RECEIVED.getCode());
                purchase.setUpdateTime(new Date());
                return purchase;
            }).collect(Collectors.toList());
            this.updateBatchById(purchaseEntities);
            // 更新采购单的状态为已领取
            for (Long id : ids) {
                List<PurchaseDetailEntity> purchaseDetails = purchaseDetailService.listByPurchaseId(id);
                List<PurchaseDetailEntity> purchaseDetailEntities = purchaseDetails.stream()
                        .filter(detail -> {
                            if (detail.getStatus().intValue() == PurchaseDetailStatusEnum.PURCHASE_DETAIL_STATUS_CREATED.getCode() || detail.getStatus().intValue() == PurchaseDetailStatusEnum.PURCHASE_DETAIL_STATUS_ASSIGNED.getCode()) {
                                return true;
                            }
                            return false;
                        }).map(detail -> {
                            PurchaseDetailEntity entity = new PurchaseDetailEntity();
                            entity.setId(detail.getId());
                            entity.setStatus(PurchaseDetailStatusEnum.PURCHASE_DETAIL_STATUS_BUYING.getCode());
                            return entity;
                        }).collect(Collectors.toList());
                purchaseDetailService.updateBatchById(purchaseDetailEntities);
            }
        });
    }

    @Transactional
    @Override
    public void purchaseDone(PurchaseDoneVo purchaseDoneVo) {
        // 1. 更新所有采购需求（项）的状态
        Optional<List<PurchaseDetailDoneVo>> itemsOptional = Optional.ofNullable(purchaseDoneVo.getItems());
        itemsOptional.ifPresent(items -> {
            boolean flag = true;
            List<PurchaseDetailEntity> purchaseDetailEntities = items.stream()
                    .map(item -> {
                        PurchaseDetailEntity detail = new PurchaseDetailEntity();
                        detail.setId(item.getItemId());
                        detail.setStatus(item.getStatus());
                        return detail;
                    }).collect(Collectors.toList());
            purchaseDetailService.updateBatchById(purchaseDetailEntities);
            for (PurchaseDetailDoneVo item : items) {
                if (item.getStatus().intValue() == PurchaseDetailStatusEnum.PURCHASE_DETAIL_STATUS_FAILED.getCode()) {
                    flag = false;
                }
            }
            // 2. 如果所有采购需求（项）的状态为已完成，则更新采购单的状态为已完成，否则更新采购单的状态为有异常。
            PurchaseEntity purchase = new PurchaseEntity();
            purchase.setId(purchaseDoneVo.getId());
            if (flag) {
                purchase.setStatus(PurchaseStatusEnum.PURCHASE_STATUS_COMPLETED.getCode());
            } else {
                purchase.setStatus(PurchaseStatusEnum.PURCHASE_STATUS_ABNORMAL.getCode());
            }
            baseMapper.updateById(purchase);
            // 3. 对所有状态为已完成的采购需求（项）更新其对应的库存数
            purchaseDetailEntities.stream()
                    .filter(detail -> {
                        if (detail.getStatus().intValue() == PurchaseDetailStatusEnum.PURCHASE_DETAIL_STATUS_COMPLETED.getCode()) {
                            return true;
                        }
                        return false;
                    }).forEach(detail -> wareSkuService.syncWareSku(detail.getSkuId(), detail.getWareId(), detail.getSkuNum()));
        });
    }

}