package com.atguigu.gulimall.ware.vo;

import lombok.Data;

/**
 * @Name PurchaseDetailDoneVo
 * @Description:
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-06 21:53:31
 **/
@Data
public class PurchaseDetailDoneVo {
    /**
     * 采购需求 ID
     */
    private Long itemId;
    /**
     * 采购需求状态
     * 3. 已完成
     * 4. 采购失败
     */
    private Integer status;
    /**
     * 采购失败原因
     */
    private String reason;
}

