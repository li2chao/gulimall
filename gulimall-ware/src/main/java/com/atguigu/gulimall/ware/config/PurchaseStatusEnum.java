package com.atguigu.gulimall.ware.config;

/**
 * @Name PurchaseStatusEnum
 * @Description: 采购单状态枚举类
 * @Author: lichao
 * @Email: li2chao@126.com
 * @Date: 2024-01-06 20:03:44
 **/
public enum PurchaseStatusEnum {
    PURCHASE_STATUS_CREATED(0, "已创建"),
    PURCHASE_STATUS_ASSIGNED(1, "已分配"),
    PURCHASE_STATUS_RECEIVED(2, "已领取"),
    PURCHASE_STATUS_COMPLETED(3, "已完成"),
    PURCHASE_STATUS_ABNORMAL(4, "有异常");

    private int code;
    private String msg;

    PurchaseStatusEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
