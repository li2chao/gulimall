package com.atguigu.gulimall.ware;

import com.atguigu.gulimall.ware.entity.WareInfoEntity;
import com.atguigu.gulimall.ware.service.WareInfoService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class GulimallWareApplicationTests {

    @Autowired
    WareInfoService wareInfoService;

    @Test
    void contextLoads() {
        //WareInfoEntity wareInfoEntity = new WareInfoEntity();
        //wareInfoEntity.setName("河南省-周口市-冷冻-001");
        //wareInfoService.save(wareInfoEntity);
        //System.out.println("保存成功");
        WareInfoEntity wareInfoEntity = wareInfoService.getById(1L);
        System.out.println(wareInfoEntity);
    }

}
